<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header("Access-Control-Allow-Methods: GET, POST, PUT");

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Sbu extends CI_Controller {
    
    public function __construct(){
        parent ::__construct();
        //load model
        $this->load->model('m_sbu'); 
    }

    public function index()
    {
        $data = array(
            'title'     => 'SBU-KENDO.UI',
            'v_sbu'     => $this->m_sbu->get_all(),
        );
        $this->load->view('v_info_pasar', $data);
    }

    public function post_sbu()
    {
        //Forcing Variable
        $created_by = 'Farras';
        // Initialization variable

        $kd_sbu                  = $this->input->post('KD_SBU');
        $ket                     = $this->input->post('KET');
        $singkatan               = $this->input->post('SINGKATAN');
        $singkatan2              = $this->input->post('SINGKATAN2');
        $ket_indo                = $this->input->post('KET_INDO');
    
        $this->m_sbu->save($kd_sbu, $ket, $singkatan, $created_by, $singkatan2, $ket_indo);
        //redirect
    }

    public function delete_sbu()
    {   
        $kd_sbu = $this->input->post('KD_SBU');
        $this->m_sbu->delete($kd_sbu);
        // $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success!
        //                                         </div>');
    }

    public function update_sbu()
    {
        //Forcing Variable
        $last_update_by = 'Yazi';
        // Initialization variable

        $kd_sbu                  = $this->input->post('KD_SBU');
        $ket                     = $this->input->post('KET');
        $singkatan               = $this->input->post('SINGKATAN');
        $singkatan2              = $this->input->post('SINGKATAN2');
        $ket_indo                = $this->input->post('KET_INDO');
    
        $this->m_sbu->update_request_kendo($kd_sbu, $ket, $singkatan, $last_update_by, $singkatan2, $ket_indo);
        //redirect
    }

    public function post_sbu_hierarchy() 
    {
            $key = $this->input->post('key');
            

            $created_by = 'Farras';
            $kd_sbu            = $key;
            $kd_k_sbu          = $this->input->post('KD_K_SBU');
            $ket               = $this->input->post('KET');
            $en                = $this->input->post('EN');

            $this->m_sbu->save_helper($kd_sbu, $kd_k_sbu, $ket, $en, $created_by);
            //redirect
    }

    public function delete_sbu_hierarchy()
    {
        // $kd_sbu     = $this->input->post('KD_SBU');
        $kd_k_sbu   = $this->input->post('KD_K_SBU');
        $this->m_sbu->delete_helper($kd_k_sbu);

    }

    public function update_sbu_hierarchy()
    {
        $filter = $this->input->post('filter');

            $last_update_by = 'Udin';
            $kd_sbu            = $this->input->post('KD_SBU');
            $kd_k_sbu          = $this->input->post('KD_K_SBU');
            $ket               = $this->input->post('KET');
            $en                = $this->input->post('EN');

            $this->m_sbu->update_helper($kd_sbu, $kd_k_sbu, $ket, $en, $last_update_by);
            //redirect
    }

    public function get_all_list() {
        $column = "kd_sbu,ket,singkatan,created_by,created_date,last_update_by,last_update_date,singkatan2,ket_indo";
        $colomn_2 = "kd_sbu,kd_k_sbu,ket,created_by,en,created_date,last_update_by,last_update_date";
    
        // paginate handle
        $take = $this->input->post('take');
        $page = $this->input->post('page');
        $skip = $this->input->post('skip');
        $pageSize = $this->input->post('pageSize');
       
        // sort
        $sort = $this->input->post('sort');
        $sort_dir = $sort[0]['dir'];
        $sort_field = $sort[0]['field'];
    
        // filter
        $filter = $this->input->post('filter');
        // $data['cekfilterdata']=$filter;
    
        if( $filter != '' ){
          
        //   filter and sorting
          $filterdata = array();
          $filterdata['field'] = $filter['filters'][0]['field'];
          $filterdata['operator'] = $filter['filters'][0]['operator'];
          $filterdata['value'] = $filter['filters'][0]['value'];
          $filterdata['logic'] = $filter['logic'];  

        //   hierarchy
          $data_temp = $this->m_sbu->query_helper( $column, $take, $skip, $sort_dir, $sort_field, $filterdata, $filterdata['value']);
          $data = $this->m_sbu->get_all_kendo( $column, $take, $skip, $sort_dir, $sort_field, $filterdata );
          $total_data = $this->m_sbu->count_all_where($filterdata);

        }else{
          $filterdata = 0;
          $data_temp = $this->m_sbu->query_helper( $column, $take, $skip, $sort_dir, $sort_field, $filterdata, $filterdata['value'] );
          $data = $this->m_sbu->get_all_kendo( $column, $take, $skip, $sort_dir, $sort_field, $filterdata );
          $total_data = $this->m_sbu->count_all();
        }
        
        $return['Result'] = $data;
        $return['Data'] = $data_temp;
        $return['CResult'] = $total_data;
    
        echo json_encode($return);
        
      }

}
