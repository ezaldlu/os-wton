<!DOCTYPE html>
<html>
<head>
    <title>OS</title>

    <link rel="stylesheet" href="assets/telerik-php/styles/kendo.common.min.css"/>
    <link rel="stylesheet" href="assets/telerik-php/styles/Tokped/kendo.custom.css" />
    <script src="assets/telerik-php/js/jquery.min.js"></script>
    <script src="assets/telerik-php/js/kendo.all.min.js"></script>
</head>
<body style="font-family: monospace">

<div id="example">
<div id="grid"></div>
<script>
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "<?php echo base_url(); ?>sbu/get_all_list",
                            dataType: "json",
                            type: "post"
                        },

                        create: {
                            url: "<?php echo base_url(); ?>Sbu/post_sbu",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read();
                            }
                        },

                        update: {
                            url: "<?php echo base_url(); ?>Sbu/update_sbu",
                            dataType: "json",
                            type: "post",
                            complete: function(e) {
                                $("#grid").data("kendoGrid").dataSource.read();
                            }
                        },

                        destroy: {
                            url: "<?php echo base_url(); ?>Sbu/delete_sbu",
                            dataType: "json",
                            type: "post"
                        }
                    },
                    
                    schema: {
                        data: "Result", function(data) {
                            return data.Result; 
                        },
                        total: function(data) {
                            return data.CResult;
                        },
                        
                        model: {
                             id: "KD_SBU",
                            fields: {
                                KD_SBU: { validation: { required: true }},
                                KET: { validation: { required: true } },
                                SINGKATAN: { validation: { required: true } },
                                SINGKATAN2: { validation: { required: true } },
                                KET_INDO: { validation: { required: true } },
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                },
                height: 550,
                groupable: true,
                sortable: true,
                filterable: {
                    extra: false
                },
                reorderable: true,
                pageable: {
                    input: true,
                    numeric: false
                },
                editable: "inline",
                toolbar: [{name: "create", text: "Add New Data"}],
                detailInit: detailInit,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                columns: [{ 
                        field: "KD_SBU",
                        title: "Kode SBU",
                        filterable: true,
                        encoded: true,
                        width: 120
                    }, { 
                        field: "KET",
                        title: "Keterangan",
                        filterable: true,
                        encoded: true  
                    }, {
                        field: "SINGKATAN",
                        title: "Singkatan",
                        filterable: true,
                        encoded: true,
                        width: 120  
                    }, {
                        field: "SINGKATAN2",
                        title: "Singkatan 2",
                        filterable: true,
                        encoded: true,
                        width: 130
                    }, {
                        field: "KET_INDO",
                        title: "Keterangan Indo",
                        filterable: true,
                        encoded: true
                    }, {
                        title: "Option",
                        width: 180,
                        command: [
                            { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                            { name: "destroy", text: "Delete" }
                        ]

                    }] 
            
            });

            function detailInit(e) {
                    $("<div/>").appendTo(e.detailCell).kendoGrid({
                        dataSource: {
                            // type: "odata",
                            transport: {
                                read: {
                                    url: "<?php echo base_url(); ?>sbu/get_all_list",
                                    dataType: "json",
                                    type: "post"
                                },

                                create: {
                                    url: "<?php echo base_url(); ?>sbu/post_sbu_hierarchy",
                                    dataType: "json",
                                    type: "post",
                                    data: { key: e.data.KD_SBU},
                                    complete: function(e) {
                                        $("#grid").data("kendoGrid").dataSource.read();
                                        
                                    }
                                    // close: onClose,
                                    // deactivate: onDeactivate
                                },

                                update: {
                                    url: "<?php echo base_url(); ?>Sbu/update_sbu_hierarchy",
                                    dataType: "json",
                                    type: "post",
                                    complete: function(e) {
                                        $("#grid").data("kendoGrid").dataSource.read();
                                        
                                    }
                                    // close: onClose,
                                    // deactivate: onDeactivate
                                },

                                destroy: {
                                    url: "<?php echo base_url(); ?>sbu/delete_sbu_hierarchy",
                                    dataType: "json",
                                    type: "post"
                                }
                            },
                            schema: {
                                data: "Data", function(data) {
                                    return data.Result; 
                                },
                                total: function(data) {
                                    return data.CResult;
                                },
                        
                            model: {
                                id: "KD_SBU",
                                fields: {
                                        //editable false;
                                        KD_SBU: { editable: false },
                                        KD_K_SBU: { validation: { required: true } },
                                        KET: { validation: { required: true } },
                                        EN: { validation: { required: true } },
                                    }
                                }
                            },
                            serverPaging: true,
                            serverSorting: true,
                            serverFiltering: true,
                            pageSize: 10,
                            filter: { field: "KD_SBU", operator: "false", value: e.data.KD_SBU }
                        },
                        filterable: {
                            extra: false
                        },
                        scrollable: false,
                        groupable: true,
                        sortable: true,
                        reorderable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
                        editable: "popup",
                        toolbar: [{name: "create", text: "Add New Data"}],
                        columns: [
                            { 
                                field: "KD_SBU", 
                                title: "KD_SBU",
                                width: 120
                            },
                            { 
                                field: "KD_K_SBU", 
                                title:"KD_K_SBU", 
                                width: 120
                            },
                            { 
                                field: "KET", 
                                title:"KET",
                                width: 330
                            },
                            { 
                                field: "EN", 
                                title: "EN"
                            },
                            {
                                title: "Option",
                                width: 180,
                                command: [
                                    { name: "edit", text: { edit: "Edit", update: "Update", cancel: "Cancel" } },
                                    { name: "destroy", text: "Delete" }
                                ]
                            }
                        ]
                    });
                }
            
        });
    </script>
</div>
</body>
</html>
