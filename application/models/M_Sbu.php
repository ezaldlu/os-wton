<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Sbu extends CI_model {

    public function get_all()
    {
        $query = $this->db->query("SELECT * FROM TB_SBU")->result();
        return $query;
    }

    public function query_helper( $column, $take, $skip, $sort_dir, $sort_field, $filterdata, $val )
    {
        $helper = "SELECT TR_K_SBU.KD_SBU, TR_K_SBU.KD_K_SBU, TR_K_SBU.KET, TR_K_SBU.EN
                                    FROM TR_K_SBU
                                    LEFT JOIN TB_SBU ON TB_SBU.KD_SBU = TR_K_SBU.KD_SBU";
        
        $query = $this->db->query("SELECT * FROM (".$helper.") WHERE KD_SBU LIKE '".$val."'")->result();

        if ($query) {
          # code...
          return $query;
        } else {
          return false;
        }
        
    }

    public function save_helper($kd_sbu, $kd_k_sbu, $ket, $en, $created_by)
    {
        $query = $this->db->query("INSERT INTO TR_K_SBU (KD_SBU,
                                                        KD_K_SBU,
                                                        KET,
                                                        CREATED_BY,
                                                        EN,
                                                        CREATED_DATE,
                                                        LAST_UPDATE_BY,
                                                        LAST_UPDATE_DATE)
                                VALUES('".$kd_sbu."', 
                                     '".$kd_k_sbu."', 
                                          '".$ket."', 
                                    '".$created_by."', 
                                            '".$en."', 
                                              SYSDATE, 
                                                NULL,
                                                NULL)")->result();
          if($query)
              return $query;
          else
              return false;   
    }

    public function save($kd_sbu, $ket, $singkatan, $created_by, $singkatan2, $ket_indo) 
    {
        $query = $this->db->query("INSERT INTO TB_SBU (KD_SBU, 
                                                          KET, 
                                                    SINGKATAN, 
                                                   CREATED_BY, 
                                                 CREATED_DATE, 
                                               LAST_UPDATE_BY, 
                                             LAST_UPDATE_DATE, 
                                                   SINGKATAN2, 
                                                     KET_INDO) 
                                VALUES('".$kd_sbu."', 
                                          '".$ket."', 
                                    '".$singkatan."', 
                                   '".$created_by."', 
                                             SYSDATE, 
                                                NULL, 
                                                NULL, 
                                   '".$singkatan2."', 
                                   '".$ket_indo."')")->result();

        if($query)
            return true;
        else
            return false;   
    }

    public function delete_helper($id)
    {
      $this->db->where('KD_K_SBU', $id);
      $query = $this->db->delete('TR_K_SBU');

      if($query)
          return true;
      else
          return false;

    }

    public function update_helper($kd_sbu, $kd_k_sbu, $ket, $en, $last_update_by)
    {
        $query = $this->db->query("UPDATE
                                        TR_K_SBU
                                      SET
                                        KET = '".$ket."',
                                        EN = '".$en."',
                                        LAST_UPDATE_BY = '".$last_update_by."',
                                        LAST_UPDATE_DATE = SYSDATE
                                      WHERE
                                        KD_K_SBU = '".$kd_k_sbu."'
                                        AND KD_SBU = '".$kd_sbu."'")->result();
        if($query)
            return $query;
        else
            return false;
    }

    public function delete($id)
    {
      $this->db->where('KD_SBU', $id);
      $query = $this->db->delete('TB_SBU');
      
      if($query)
          return true;
      else
          return false;


    }

    public function update_request_kendo($kd_sbu, $ket, $singkatan, $last_update_by, $singkatan2, $ket_indo) {
        $query = "UPDATE
                          TB_SBU
                      SET
                          KET = '".$ket."',
                          SINGKATAN = '".$singkatan."',
                          LAST_UPDATE_BY = '".$last_update_by."',
                          LAST_UPDATE_DATE = SYSDATE,
                          SINGKATAN2 = '".$singkatan2."',
                          KET_INDO = '".$ket_indo."'
                      WHERE
                          KD_SBU = '".$kd_sbu."'";
                          
        if($this->db->query($query) != FALSE && $this->db->query($query)->num_rows() > 0) {
            return $this->db->query($query)->result();
        } 
    }
  
  public function count_all() {
    $count = $this->db->count_all("TB_SBU");
    $query = $this->db->last_query();
    return $count;
  }

  public function count_all_where($filterdata) {
    if( isset($filterdata['operator']) ) {
      if( $filterdata['operator'] == 'eq' ) {
        $this->db->where($filterdata['field'], $filterdata['value']);
      }
      elseif( $filterdata['operator'] == 'neq' ) {
        $field = $filterdata['field'] . ' !=';
        $this->db->where($field, $filterdata['value']);
      }
      elseif( $filterdata['operator'] == 'startswith' ) {
        $this->db->like($filterdata['field'], $filterdata['value'], 'after');
      }
      elseif( $filterdata['operator'] == 'contains' ) {
        $this->db->like($filterdata['field'], $filterdata['value'], 'both');
      }
      elseif( $filterdata['operator'] == 'doesnotcontain' ) {
        $this->db->not_like($filterdata['field'], $filterdata['value']);
      }
      elseif( $filterdata['operator'] == 'endswith' ) {
        $this->db->like($filterdata['field'], $filterdata['value'],'before');
      }
    }
    $this->db->from("TB_SBU");
    $count = $this->db->count_all_results();
    return $count;
  }

  public function get_all_kendo( $column, $take, $skip, $sort_dir, $sort_field, $filterdata ) {

    if( isset( $sort_dir ) ){

      if( isset($filterdata) ){

        $this->db->order_by($sort_field, $sort_dir);
        $this->db->limit($take,$skip);

        if( isset($filterdata['operator']) ) {
          
          if( $filterdata['operator'] == 'eq' ) {
            $this->db->where($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'neq' ) {
            $field = $filterdata['field'] . ' != ';
            $this->db->where($field, $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'startswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'after');
          }
          elseif( $filterdata['operator'] == 'contains' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'both');
          }
          elseif( $filterdata['operator'] == 'doesnotcontain' ) {
            $this->db->not_like($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'endswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'],'before');
          }
        }

        $data = $this->db->get('TB_SBU');
      }else{
        $this->db->order_by($sort_field, $sort_dir);
        $this->db->limit($take,$skip);
        $data = $this->db->get('TB_SBU');
      }
      
    }else{
      
      if( $filterdata != 0 ){

        if( isset($filterdata['operator']) ) {
          if( $filterdata['operator'] == 'eq' ) {
            $this->db->where($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'neq' ) {
            $field = $filterdata['field'] . ' != ';
            $this->db->where($field, $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'startswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'after');
          }
          elseif( $filterdata['operator'] == 'contains' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'both');
          }
          elseif( $filterdata['operator'] == 'doesnotcontain' ) {
            $this->db->not_like($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'endswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'],'before');
          }
        }
        
        $this->db->limit($take,$skip);
        $data = $this->db->get('TB_SBU');
      }else{
        $this->db->limit($take,$skip);
        $data = $this->db->get('TB_SBU');
      }

    }
    return $data->result();
  }

  public function get_all_hierarchy_kendo( $column_2, $take, $skip, $sort_dir, $sort_field, $filterdata ) {

    if( isset( $sort_dir ) ){

      if( isset($filterdata) ){

        $this->db->order_by($sort_field, $sort_dir);
        $this->db->limit($take,$skip);

        if( isset($filterdata['operator']) ) {
          
          if( $filterdata['operator'] == 'eq' ) {
            $this->db->where($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'neq' ) {
            $field = $filterdata['field'] . ' != ';
            $this->db->where($field, $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'startswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'after');
          }
          elseif( $filterdata['operator'] == 'contains' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'both');
          }
          elseif( $filterdata['operator'] == 'doesnotcontain' ) {
            $this->db->not_like($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'endswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'],'before');
          }
        }

        $data = $this->db->get('KD_K_SBU');
      }else{
        $this->db->order_by($sort_field, $sort_dir);
        $this->db->limit($take,$skip);
        $data = $this->db->get('KD_K_SBU');
      }
      
    }else{
      
      if( $filterdata != 0 ){

        if( isset($filterdata['operator']) ) {
          if( $filterdata['operator'] == 'eq' ) {
            $this->db->where($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'neq' ) {
            $field = $filterdata['field'] . ' != ';
            $this->db->where($field, $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'startswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'after');
          }
          elseif( $filterdata['operator'] == 'contains' ) {
            $this->db->like($filterdata['field'], $filterdata['value'], 'both');
          }
          elseif( $filterdata['operator'] == 'doesnotcontain' ) {
            $this->db->not_like($filterdata['field'], $filterdata['value']);
          }
          elseif( $filterdata['operator'] == 'endswith' ) {
            $this->db->like($filterdata['field'], $filterdata['value'],'before');
          }
        }
        
        $this->db->limit($take,$skip);
        $data = $this->db->get('KD_K_SBU');
      }else{
        $this->db->limit($take,$skip);
        $data = $this->db->get('KD_K_SBU');
      }

    }
    return $data->result();
  }


}
